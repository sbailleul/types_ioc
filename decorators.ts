import CoreContainer from "./CoreContainer";
import {ClassType} from "./ClassType";
import {InjectionInstance} from "./InjectionInstance";
import {InjectionProperty} from "./InjectionProperty";

/**
 * Class decorator trigerred when class is referenced for example during import
 * @param constructor Class constructor targeted by injection, return constructor with properties to inject set
 */
export function InjectionTarget<T>(constructor: ClassType<T>) {
    CoreContainer.injectProperties(new InjectionInstance({class: constructor, id:0}));
    return constructor;
}

/**
 * Property decorator trigerred when class is referenced for example during import
 * @param type
 */
// Specify class properties are injected by ioc
export function InjectedProperty(type: ClassType<object>): any {

    return function (target: any, propertyKey: string) {
        CoreContainer.addInjectedProperty(target.constructor, new InjectionProperty({propertyKey, class: type}));
    }

}

// Specify than type are injectable by ioc
export function Injectable(constructor: ClassType<object>) {
    CoreContainer.addInjectable(constructor);
}
