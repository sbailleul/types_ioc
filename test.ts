import CoreContainer from "./CoreContainer";
import {Injectable, InjectedProperty, InjectionTarget} from "./decorators";
import {InjectionProperty} from "./InjectionProperty";


abstract class BaseA{

}
@Injectable
class A extends BaseA{
    private readonly _variable: string;
    constructor(variable: string) {
        super();
        this._variable = variable;
    }
}

abstract class BaseB{

}
@Injectable
@InjectionTarget
class B extends BaseB{
    @InjectedProperty(BaseA)
    private readonly _a: BaseA
}

@InjectionTarget
class C {
    @InjectedProperty(BaseB)
    private readonly _b: BaseB;
}
CoreContainer.bind(BaseA, {instance: new A("salut")});
CoreContainer.bind(BaseB, {class: B});
CoreContainer.resolve();
const a = CoreContainer.getInstance(BaseA);
console.log(a);
