import {ClassType} from "./ClassType";
import {DependencyClass} from "./DependencyClass";

/**
 *  Contain name and type of an injected property
 */
export class InjectionProperty extends DependencyClass {
    propertyKey: string

    constructor(obj: { class: ClassType<any>, propertyKey: string } ) {
        super(obj);
        this.propertyKey = obj?.propertyKey;
    }
}

