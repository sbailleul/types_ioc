import {DependencyClass} from "./DependencyClass";
import {ClassType} from "./ClassType";

export class InjectionParameter extends DependencyClass{
    parameterIndex: number;
    propertyKey: string;


    constructor(obj: { class: ClassType<any>, parameterIndex: number, propertyKey: string }) {
        super(obj);
        this.parameterIndex = obj?.parameterIndex;
        this.propertyKey = obj?.propertyKey;
    }
}
