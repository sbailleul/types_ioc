import {ClassType} from "./ClassType";

export class DependencyClass {
    classPrototype: ClassType<any>;
    instance?: any;


    constructor(obj: { class?: ClassType<any>, instance?: any }) {
        this.classPrototype = obj?.class ?? obj?.instance.constructor;
        this.instance = obj.instance;
    }

    public get className() {
        return this.classPrototype.name;
    }
}
