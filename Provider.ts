import {ClassType} from "./ClassType";
import {DependencyClass} from "./DependencyClass";

export interface IProviderOption {
    tags?: string[] | string | undefined;
    class?: ClassType<any>;
    instance?: any
}

export class Provider extends DependencyClass{
    tags: string[];

    constructor(obj: IProviderOption) {
        super(obj);
        if(obj.tags){
            if(!Array.isArray(obj.tags)){
                obj.tags = [obj?.tags];
            }
        } else {
            this.tags = [];
        }
        this.classPrototype = obj?.class ?? obj?.instance.constructor;
    }

}
