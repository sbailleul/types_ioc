import "reflect-metadata";
import {InjectionProperty} from "./InjectionProperty";
import {ClassType} from "./ClassType";
import {InjectionParameter} from "./InjectionParameter";
import {InjectionInstance} from "./InjectionInstance";
import {IProviderOption, Provider} from "./Provider";
import {MapUtil} from "./utils/MapUtil";

export default class CoreContainer {

    public static injectionsTags = new Set<string>();
    private static _typedProviders = new Map<ClassType<object>, Provider>();
    private static _injectables: ClassType<object>[] = [];
    private static _injectionTargets = new Map<string, { properties: InjectionProperty[], ctrParam: InjectionParameter[] }>();
    private static _awaitingInstances = new Map<string, InjectionInstance[]>();
    private static _instanceCounter = 0;
    private static _resolvedInstances = new Map<string, InjectionInstance>();

    /**
     * Bind new provider in _typedProvides Map
     * @param type Custom type used to retrieve constructors in _typedProvides
     * @param provider Class constructor call during injection
     */
    public static bind(type: ClassType<any>, provider: IProviderOption | Provider) {
        let providerInstance: Provider;
        if (provider instanceof Provider) {
            providerInstance = provider;
        } else {
            providerInstance = new Provider(provider);
        }

        if (!this.isInjectable(providerInstance))
            throw new Error(`${providerInstance.className} is not injectable`);
        if (this._typedProviders.has(type)) {
            console.warn(`Type ${type.name} already mapped`);
        }
        this._typedProviders.set(type, providerInstance);
    }


    /**
     * Add injectable constructor to check during binding if class to bind is injectable
     * @param classPrototype Constructor to stock in _injectables
     */
    public static addInjectable(classPrototype: ClassType<object>) {
        this._injectables.push(classPrototype);
    }

    /**
     * Injection tags permit to choice which classe implementation choose for ambiguous choices
     * @param tags
     */
    public static addInjectionTags(...tags: string[]) {
        tags.forEach(t => this.injectionsTags.add(t));
    }

    /**
     * Reference class properties to inject later in injectProperties
     * @param destClass Destination class for injection
     * @param property Target property for injection
     */
    public static addInjectedProperty(destClass: ClassType<object>, property: InjectionProperty) {

        const className = this.initClassInjectionTypes(destClass);
        this._injectionTargets.get(className)!.properties.push(property);
    }


    /**
     * Inject properties to class instance, look in _injectionTargets if class is referenced, if it's true try to find corresponding constructor to call to init property.
     * @param injectionInstance Class prototype
     */
    public static injectProperties(injectionInstance: InjectionInstance, isResolving = false) {
        const className = injectionInstance.className;
        const propertiesToInject = this._injectionTargets.get(className)?.properties;
        if (!propertiesToInject) {
            console.warn(`No properties to inject for ${className}`);
        } else {
            if (isResolving) {
                this.resolveAwaitingProperties(propertiesToInject);
            }
            if (!this.hasAllProperties(propertiesToInject)) {
                this.addAwaitingInstance(injectionInstance, className);
                return;
            }
            this.setPropertiesToInstance(propertiesToInject, injectionInstance);
        }
        this.removeResolvedInstance(className, injectionInstance.id);
    }

    private static resolveAwaitingProperties(properties: InjectionProperty[]) {
        properties.forEach(p => {
            const awaitingInstances = this._awaitingInstances.get(p.className);
            if (awaitingInstances) {
                awaitingInstances.forEach(awaitingInstance => this.injectProperties(awaitingInstance, true));
            }
        })
    }

    /***
     * Keep non resolved instances in Map to resolve them after
     * @param injectionInstance
     * @param className
     */
    private static addAwaitingInstance(injectionInstance: InjectionInstance, className: string) {
        if (injectionInstance.id) return;

        this._instanceCounter++;
        injectionInstance.id = this._instanceCounter;
        MapUtil.addElementToArrayMap(this._awaitingInstances, className, injectionInstance)
    }


    /**
     * Try to get provider for corresponding instance property and instantiate it
     * @param propertiesToInject
     * @param injectionInstance
     */
    private static setPropertiesToInstance(propertiesToInject: InjectionProperty[], injectionInstance: InjectionInstance) {
        propertiesToInject.forEach(property => {
            const provider = this._typedProviders.get(property.classPrototype);
            if (this.isValidProvider(provider)) {
                Reflect.set(injectionInstance.classPrototype, property.propertyKey, provider?.instance ?? new provider!.classPrototype());
            } else {
                console.warn(`No provider of type ${property.classPrototype.name} found for property ${property.propertyKey}`);
            }
        });
    }

    /**
     * Check for each properties if corresponding provider is register in container
     * @param properties
     */
    private static hasAllProperties(properties: InjectionProperty[]): boolean {
        return properties.every(property => this._typedProviders.has(property.classPrototype));
    }

    /**
     * Check if provider can be used by container, if tags are present on this provider check if tags are registered in container
     * @param provider
     */
    private static isValidProvider(provider: Provider | undefined) {
        if (!provider) return false;
        if (!provider.tags || provider.tags.length === 0) return true;

        return provider.tags.some(t => this.injectionsTags.has(t));
    }

    /**
     * Resolve all remaining instances
     */
    public static resolve() {
        this._awaitingInstances.forEach((prototypes, className) => {
            prototypes.forEach(p => this.injectProperties(p));
        })
    }

    public static getInstance<TInstance>(searchedClass: ClassType<any>): TInstance {
        const c = searchedClass as ClassType<any>;
        const provider = this._typedProviders.get(c);
        if (provider?.instance) {
            return provider.instance;
        }
        const instance = this._resolvedInstances.get(c.name);
        if (!instance) {
            console.warn(`Instance ${c.name} doesn't exist trying to call searchedClass constructor...`);
            return new searchedClass();
        }
        const constructedInstance = new instance.class();
        Object.keys(instance?.classPrototype)
            .filter(k => k !== "constructor" && k !== "__proto__")
            .forEach(k => {
                constructedInstance[k] = instance?.classPrototype[k]
            })
        return constructedInstance;
    }

    /**
     * Remove instance from awaiting instances
     * @param className
     * @param id
     */
    private static removeResolvedInstance(className: string, id: number) {
        if (this._awaitingInstances.has(className)) {
            const awaitingInstance = this._awaitingInstances.get(className);
            const instances = awaitingInstance?.filter(instance => instance.id !== id);
            if (!instances || instances.length === 0) {
                this._awaitingInstances.delete(className);
                if (awaitingInstance) {
                    this._resolvedInstances.set(className, awaitingInstance[0]);
                }
                return;
            }
            this._awaitingInstances.set(className, instances);
        }
    }


    /**
     * Check if destClass is in _injectionTargets and add it if not
     * @param destClass Check if class is in _injectionTargets if not add it
     * @return Class name from destClass
     */
    private static initClassInjectionTypes(destClass: ClassType<object>): string {
        const className = destClass.name;
        if (!this._injectionTargets.has(className)) {
            this._injectionTargets.set(className, {properties: [], ctrParam: []});
        }
        return className;
    }

    /**
     * Test if class constructor exist in _injectables
     * @param provider Class to verify
     */
    private static isInjectable(provider: Provider): boolean {
        return this._injectables.find(i => i.name === provider.className) !== undefined;
    }

}
