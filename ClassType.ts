/**
 * Type used to call class constructors
 */
type ConstructorType<T> = new (...args: any[]) => T;
export type  ClassType<T> =  ConstructorType<T> | ReturnType<any>;
