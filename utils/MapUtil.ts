export class MapUtil {

    public static addElementToArrayMap<K, V>(map: Map<K, V[]>, mapKey: K, arrElement: V) {
        if (!map.has(mapKey)) {
            map.set(mapKey, []);
        }
        map.get(mapKey)?.push(arrElement);
    }
}
