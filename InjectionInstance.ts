import {ClassType} from "./ClassType";
import {DependencyClass} from "./DependencyClass";

export class InjectionInstance extends DependencyClass{
    id: number;
    private readonly _className: string;
    public class: any;

    constructor(obj: { class: ClassType<any>, id: number }) {
        super(obj);
        this.id = obj?.id;
        this._className = obj?.class.name;
        this.classPrototype = obj?.class.prototype;
        this.class = obj?.class;
    }

    get className(): string {
        return this._className;
    }
}
